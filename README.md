# Neteja

</p>Este es el Script de Voro MV, para limpiar el sistema del pingüino, es valido para los gestores de paquetes <STRONG>APT,DNF,PACMAN y ZYPPER</STRONG>.</p>
<p>Se llama <STRONG>NETEJA</STRONG> y está en su 2ª versión. Con él, vamos a actualizar el sistema , Flatpak y/o Snap, si los tienes instalados.Finalmente eliminaremos toda la basurilla de nuestro sistema operativo.</p>

### Nota:
En el proceso, tendrás que poner tu contraseña de usuario.


## Funcionamiento del Script

<p>Para utilizar Neteja lo único que tendras que hacer es descargarlo en un directorio de tu sistema y darle permiso de ejecución al fichero neteja.sh.</p>
<p>
Al ejecutar el script sin paremetros emepzara a identificar que gestor de paqetes tiene tu sistema y empezara a actulizarlo y a limpiarlo despues de que le indiques la contraseña del usuario root.
</p>

```bash
./neteja.sh #Sin  parametros actualizará y limpiará el sistema
```
<p>El script cuenta con dos parametros, como podemos ver en la explicación siguiente. Cuando se le indica los parametros solo dará la información de lal versión del script y una pequeña ayuda, pero en ningún caso actualizará ni limpiará el sistema.</p>

```bash
./neteja.sh [-v][--version] [-h][--help] [-D][--delete]

# [-v][--version]: Argumento opcional que indica la versión del script.

# [-h][--help]: Argumento opcional que muestra la ayuda del script.

# [-D][--delete]: Argumento opcional que sirve para indicar un directorio donde borrará los ficheros vacios que se encuentre en él.
```
