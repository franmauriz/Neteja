function presentacion(){
    sleep 2s
    echo
    echo Hola!!
    echo
    sleep 1s
    echo  Este es el Script de Voro MV, para limpiar el sistema.
    echo
    sleep 1s
    echo  Se llama NETEJA
    echo  y está en su 2ª versión    
    echo                                
    sleep 2s
    echo  Con él, vamos a actualizar el sistema.
    echo  También Flatpak y Snap, si los tienes instalados.
    echo  Finalmente eliminaremos toda la basurilla. ¿Ok?
    sleep 1s
    echo  En el proceso, tendrás que poner tu contraseña de usuario.
    echo
    sleep 3s
}

function borrarFicherosVacios(){
    echo "Directorio donde eliminaremos los ficheros vacios: $dir"
    ficheros=$(ls $dir)
    read -p "Estas segur@ de querer borrar el contenido de $dir? (s/n)" sn
    case $sn in
        [Ss]*)
            for fichero in ${ficheros[@]}
            do
                if [ -d $dir/$fichero ] || [ -s $dir/$fichero ]
                then
                    echo "no tengo nada que hacer con ${fichero}"
                else
                    echo "Vamos a eliminar el fichero ${fichero}"
                    rm -f $dir/$fichero
                fi
            done;;
        [Nn]*) exit;;
        *)echo "Por favor, pulsa s o n.";;
    esac
}


function ayuda(){
    echo
    echo Neteja
    echo
    echo ./neteja.sh [-v][--version] [-h][--help]
    echo
    echo [-v][--version]: Argumento opcional que indica la versión del script.
    echo
    echo [-h][--help]: Argumento opcional que muestra la ayuda del script.
    echo
    echo [-D][--delete]: Argumento opcional que sirve para indicar un directorio donde borrará los ficheros vacios que se encuentre en él.
    echo
    echo Descripción Script:
    echo
    echo Con él, vamos a actualizar el sistema.
    echo También Flatpak y Snap, si los tienes instalados.
    echo Finalmente eliminaremos toda la basurilla.
    echo
    echo Nota:
    echo
    echo En el proceso, tendrás que poner tu contraseña de usuario.
    echo
}

function papelera(){
     while true; do
        echo
        read -p "Estas segur@ de querer borrar el contenido? (s/n)" sn
        case $sn in
            [Ss]* )  rm -rf ~/.local/share/Trash/*; break;;
            [Nn]* ) exit;;
            * ) echo "Por favor, pulsa s o n.";;
        esac
    done
}


function despedida(){
    sleep 2s
    echo  
    echo Ya hemos terminado.
    sleep 2s
    echo Ya tienes tu sistema limpio.
    sleep 2s
    echo
    echo Este Script es público, está creado por la comunidad de Voro MV, y disfruta de las 4 libertades:
    echo La libertad de ejecutar el software como te plazca y con cualquier objetivo.
    echo La libertad de estudiar como funciona el programa y cambiarlo a tu gusto.
    echo La libertad de poder redistribuir copias del programa a los demás.
    echo La libertad de poder distribuir también tus mejoras al programa original.
    echo
    echo
    sleep 2s
}