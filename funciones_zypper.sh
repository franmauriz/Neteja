function fZypper(){
    echo Actualizamos el sistema y flatpak/snap, si los tenemos instalados
    echo ""
    sudo zypper update -y && flatpak upgrade -y 
    echo
    sleep 1s
    echo Limpiamos cachés...
    echo
    sudo zypper clean ;
    sleep 1s;
    echo
    echo Borramos directorios temporales
    echo "";
    sudo rm -rf /tmp/*;
    rm -vfr /tmp/* >/dev/null 2>&1 && rm -vfr /var/tmp/* >/dev/null 2>&1;
    sleep 1s;
    echo Por último, vamos a vaciar la Papelera.
    sleep 1s
    echo Ten en cuenta que no podrás recuperar nada de lo que había en ella.
}