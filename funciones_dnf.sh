function fDnf(){
    echo Actualizamos el sistema. También Flatpak/Snap, si los tienes instalados.
    sleep 2s
    echo "";
    sudo dnf update -y  && sudo dnf upgrade -y && sudo flatpak update && sudo snap refresh
    sleep 2s
    echo
    echo Ahora borramos versiones antiguas de programas, Kernel, cachés...
    echo 
    sleep 2s
    sudo dnf clean all && sudo dnf autoremove
    sleep 1s;
    echo 
    echo Borramos directorios temporales.
    sleep 2s
    echo "";
    sudo rm -rf /tmp/*;
    rm -vfr /tmp/* >/dev/null 2>&1 && rm -vfr /var/tmp/* >/dev/null 2>&1;
    sleep 2s;
    echo Por último, vamos a vaciar la Papelera.
    sleep 1s
    echo Ten en cuenta que no podrás recuperar nada de lo que había en ella.
}