#!/bin/bash
source funciones_comunes.sh
source funciones_apt.sh
source funciones_dnf.sh
source funciones_pacman.sh
source funciones_zypper.sh

param=$1
dir=$2
version="2"
if [ $param = '--version' ] || [ $param = '-v' ] 
then
        echo Neteja ${version} versión
elif [ $param = '--delete' ] || [ $param = '-D' ]
then
        borrarFicherosVacios "$dir"
elif [ $param = '--help' ] || [ $param = '-h' ] 
then
        ayuda
else
        presentacion
        while true; do
                
                ####  AVERIGUAMOS QUÉ PAQUETE USA
                echo
                echo  Pero antes de actualizar y limpiar el sistema, descubramos qué gestor de paquetes usas
                echo
                #       1: APT    para Debian, Ubuntu y derivadas
                #       2: DNF    para Fedora
                #       3: PACMAN para Arch
                #       4: ZYPPER para Suse
                #       n: Salir 
                echo "Comprobando..."
                sleep 2s    
                
                apt --help > /dev/null 2>&1
                if [ $? == 0 ]
                        then  
                                echo "Tu sistema tiene como gestor APT"
                                sn=1
                fi
                
                dnf --help > /dev/null 2>&1
                if [ $? == 0 ] 
                        then  
                                echo "Tu sistema tiene como gestor DNF"
                                sn=2
                fi
        
                pacman --help > /dev/null 2>&1
                if [ $? == 0 ] 
                        then  
                                echo "Tu sistema tiene como gestor PACMAN"
                                sn=3
                fi
        
                zypper --help > /dev/null 2>&1
                if [ $? == 0 ] 
                        then  
                                echo "Tu sistema tiene como gestor ZYPPER"
                                sn=4
                fi
        
                read -p "¿Quieres continuar (s/n)? " qc
        
                case $qc in
                        [Ss]* ) echo "Continuemos";;
                        [Nn]* ) exit;;
                        * ) echo "Por favor, pulsa s o n.";;
                esac
             
                case $sn in
                        [1]* )  fApt
                                papelera
                                sleep 3s
                                echo  
                                break;;             
                        [2]* )  fDnf
                                papelera
                                sleep 3s
                                echo  
                                break;;  
                        [3]* )  fPacman
                                papelera
                                sleep 3s
                                echo  
                                break;;
                        [4]* )  fZypper
                                papelera
                                sleep 3s
                                echo  
                                break;;
                        [Nn]* ) exit;;
                        * ) echo "Este script no es compatible con tu gestor de paqueteria.";;
                esac
        done
        despedida
fi