function fPacman(){
    echo
    echo Actualizamos el sistema. También Flatpak/Snap, si los tienes instalados.
    echo También borramos versiones antiguas de programas, Kernel, cachés...
    echo
    sleep 2s
    sudo pacman -Syuy -y && flatpak upgrade -y && sudo pacman -Scc -y;
    sleep 1s;
    echo
    echo Borramos directorios temporales.
    echo 
    sudo rm -rf /tmp/*;
    rm -vfr /tmp/* >/dev/null 2>&1 && rm -vfr /var/tmp/* >/dev/null 2>&1;
    sleep 1s;
    echo Por último, vamos a vaciar la Papelera.
    sleep 1s
    echo Ten en cuenta que no podrás recuperar nada de lo que había en ella.
}